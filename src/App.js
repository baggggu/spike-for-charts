import React from "react";
import { Link } from "react-router-dom";

import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h4>Chart libraries:</h4>
        <nav>
          <Link to="/carbon">&gt; Carbon</Link>
          <br />
          <Link to="/d3">&gt; D3</Link>
        </nav>
      </header>
    </div>
  );
}

export default App;
