import React, { useState, useEffect } from "react";
import LineChart from "./LineChart";
import BarChart from "./BarChart";
import RadialChart from "./RadialChart";

function App() {
  const [temps, setTemps] = useState({});
  const [city, setCity] = useState("sf");
  const [data, setData] = useState(temps[city]);

  useEffect(() => {
    Promise.all([
      fetch(`${process.env.PUBLIC_URL}/sf.json`),
      fetch(`${process.env.PUBLIC_URL}/ny.json`),
      fetch(`${process.env.PUBLIC_URL}/am.json`),
    ])
      .then((responses) => Promise.all(responses.map((resp) => resp.json())))
      .then(([sf, ny, am]) => {
        sf.forEach((day) => (day.date = new Date(day.date)));
        ny.forEach((day) => (day.date = new Date(day.date)));
        // am.forEach(day => day.date = new Date(day.date));

        setTemps({ sf, ny, am });
      });
  }, []);

  const updateCity = (e) => {
    setCity(e.target.value);
  };

  useEffect(() => {
    setData(temps[city]);
  }, [city, temps]);

  return (
    <div className="App">
      <h2>
        2021 Average Balances
        <select name="city" onChange={updateCity}>
          {[
            { label: "Savings", value: "sf" },
            { label: "Current", value: "ny" },
            // {label: 'Amsterdam', value: 'am'},
          ].map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      </h2>
      <LineChart data={data} />
      <BarChart data={data} />
      <RadialChart data={data} />
    </div>
  );
}

export default App;
