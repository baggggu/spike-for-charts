import React from "react";
import DepositsOutgoingsBar from "./DepositsOutgoingsBar";
import DepositsOutgoingsChart from "./DepositsOutgoingsChart";
import Summary from "./Summary";
// import { getLCP, getFID, getCLS } from 'web-vitals';

import "../App.css";

function App() {
  // useEffect(() => {
  //   getCLS(console.log);
  //   getFID(console.log);
  //   getLCP(console.log);
  // }, []);

  return (
    <div className="App">
      <div className="Chart-grid-column">
        <DepositsOutgoingsBar />
        <div className="Chart-grid-row">
          <Summary />
          <DepositsOutgoingsChart />
        </div>
      </div>
    </div>
  );
}

export default App;
