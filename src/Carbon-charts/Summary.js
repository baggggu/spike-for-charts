import React from "react";
import "@carbon/charts/styles.css";

function Summary() {
  return (
    <div className="Chart-summary">
      <div className="Chart-summary-title">Deposits minus Outgoings</div>
      <div>
        <div className="Chart-summary-amount">-£208.01</div>
        <div className="Chart-summary-amount-label">Average Per Month</div>
      </div>
      <div>
        <div className="Chart-summary-amount">-£2,496.09</div>
        <div className="Chart-summary-amount-label">Overall</div>
      </div>
    </div>
  );
}

export default Summary;
