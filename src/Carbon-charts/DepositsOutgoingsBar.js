import React from "react";
import { GroupedBarChart } from "@carbon/charts-react";
import "@carbon/charts/styles.css";
// Or
// import "@carbon/charts/styles/styles.scss";

// IBM Plex should either be imported in your project by using Carbon
// or consumed manually through an import
// import "./ibm-plex-font.css";

class App extends React.Component {
  state = {
    data: [
      {
        group: "Deposits",
        key: "08 2021",
        value: 65000,
      },
      {
        group: "Deposits",
        key: "09 2021",
        value: 29123,
      },
      {
        group: "Deposits",
        key: "10 2021",
        value: 35213,
      },
      {
        group: "Deposits",
        key: "11 2021",
        value: 51213,
      },
      {
        group: "Deposits",
        key: "12 2021",
        value: 43000,
      },
      {
        group: "Outgoings",
        key: "08 2021",
        value: -32432,
      },
      {
        group: "Outgoings",
        key: "09 2021",
        value: -21312,
      },
      {
        group: "Outgoings",
        key: "10 2021",
        value: -56456,
      },
      {
        group: "Outgoings",
        key: "11 2021",
        value: -21312,
      },
      {
        group: "Outgoings",
        key: "12 2021",
        value: -34234,
      },
    ],
    options: {
      title: "Deposits vs Outgoings",
      axes: {
        left: {
          mapsTo: "value",
        },
        bottom: {
          scaleType: "labels",
          mapsTo: "key",
        },
      },
      height: "350px",
    },
  };

  render = () => (
    <GroupedBarChart data={this.state.data} options={this.state.options} />
  );
}
export default App;
