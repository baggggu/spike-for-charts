import React, { useState, useEffect } from "react";
import { DonutChart } from "@carbon/charts-react";
import "@carbon/charts/styles.css";
// Or
// import "@carbon/charts/styles/styles.scss";

// IBM Plex should either be imported in your project by using Carbon
// or consumed manually through an import
// import "./plex-and-carbon-components.css";

function DepositsOutgoingsChart() {
  const [data, setData] = useState([]);
  const [options, setOptions] = useState({});

  useEffect(() => {
    setOptions({
      title: "Deposits vs Outgoings",
      resizable: true,
      legend: {
        alignment: "center",
      },
      donut: {
        center: {
          label: "Total",
        },
        alignment: "center",
      },
      height: "350px",
    });

    setData([
      {
        group: "Outgoings",
        value: 1122346.6,
      },
      {
        group: "Deposits",
        value: 1124842.69,
      },
    ]);
  }, []);

  return <DonutChart data={data} options={options} />;
}

export default DepositsOutgoingsChart;
